import { changeSample } from './changeSample';
import { getValue } from './getValue';
import { enterValue } from './enterValue';
import { sumValue } from './sumValue';
import { subValue } from './subValue';
import { timesValue } from './timesValue';
import { divValue } from './divValue';
import { sqrValue } from './sqrValue';
import { sumAllValue } from './sumAllValue';
import { undoValue } from './undoValue';
import { dotValue } from './dotValue';

export { changeSample, getValue, enterValue, sumValue, subValue, timesValue, divValue, sqrValue, sumAllValue, undoValue, dotValue };
