type DotAction = {
    type: 'DOT_VALUE';
  };
  
  export const dotValue = (): DotAction => ({
    type: 'DOT_VALUE',
  });
  