type UndoAction = {
    type: 'UNDO_VALUE';
};
  
export const undoValue = (): UndoAction => ({
    type: 'UNDO_VALUE',
});
  