import { combineReducers } from 'redux';

import { sampleReducer } from './sampleReducer';
import { calcReducer } from './calcReducer';
import { higherOrderReducer } from './higherOrderReducer';

export const rootReducer = higherOrderReducer(
  combineReducers({
    sample: sampleReducer,
    calcReducer: calcReducer,
  })
);
