import { Reducer } from 'redux';
import { undoValue } from 'state/actions';
import { AppAction } from '../AppAction';

type SampleState = {
  stack: number[];
  history: number[][];
  currentValue: number;
  dot: boolean;
};

const initialState: SampleState = {
  stack: [],
  history: [],
  currentValue: 0,
  dot: false,
};

export const calcReducer: Reducer<SampleState, AppAction> = (
  state = initialState,
  action
) => {
  let first: number | undefined;
  let second: number | undefined;
  let result: number = 0
  let auxHistory: number[][] = []

  switch (action.type) {
    case 'GET_VALUE':
        let newNum: number = 0
        if (state.dot == true) {
          newNum = state.currentValue + action.payload / 10
        } else {
          newNum = state.currentValue * 10 + action.payload
        }

        return { stack: state.stack, history: state.history, currentValue: newNum, dot: false };
    case 'ENTER_VALUE':
      return { stack: [state.currentValue, ...state.stack], history: [...state.history, [...state.stack]], currentValue: 0, dot: false};
    case 'SUM_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      first = state.stack.shift()
      
      if (state.currentValue != 0 || state.stack.length == 0) {
        second = state.currentValue
        state.currentValue = 0
      } else {
        second =  state.stack.shift()
      }
      
      if (first != undefined && second != undefined) {
        result = first + second
      }

      return { stack: [result, ...state.stack], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'SUB_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      first = state.stack.shift()
      
      if (state.currentValue != 0 || state.stack.length == 0) {
        second = state.currentValue
        state.currentValue = 0
      } else {
        second =  state.stack.shift()
      }

      if (first != undefined && second != undefined) {
        result = second - first
      }

      return { stack: [result, ...state.stack], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'TIMES_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      first = state.stack.shift()

      if (state.currentValue != 0 || state.stack.length == 0) {
        second = state.currentValue
        state.currentValue = 0
      } else {
        second =  state.stack.shift()
      }

      if (first != undefined && second != undefined) {
        result = second * first
      }

      return { stack: [result, ...state.stack], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'DIV_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      first = state.stack.shift()

      if (state.currentValue != 0 || state.stack.length == 0) {
        second = state.currentValue
        state.currentValue = 0
      } else {
        second =  state.stack.shift()
      }

      if (first != undefined && second != undefined) {
        result = second / first
      }

      return { stack: [result, ...state.stack], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'SQR_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      first = state.stack.shift()

      if (first != undefined) {
        result = Math.sqrt(first)
      }

      return { stack: [result, ...state.stack], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'SUMALL_VALUE':
      if (state.stack.length < 1) {
        return state;
      }

      auxHistory = [...state.history, [...state.stack]]

      result = state.stack.reduce((curr, prev) => curr + prev)

      return { stack: [result], history: auxHistory, currentValue: state.currentValue, dot: false};
    case 'UNDO_VALUE':
      if (state.history.length < 1) {
        return state;
      }

      let newStack = state.history.pop()
      if (newStack == undefined) {
        return state
      }

      return { stack: [...newStack], history: [...state.history], currentValue: state.currentValue, dot: false};

    case 'DOT_VALUE':
      return { stack: [...state.stack], history: [...state.history], currentValue: state.currentValue, dot: true};
    default:
      return state;
  }
};
