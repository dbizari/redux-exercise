import { useDispatch, useSelector } from 'react-redux';

import { changeSample } from 'state/actions/changeSample';
import { getValue } from 'state/actions/getValue';
import { enterValue } from 'state/actions/enterValue';
import { sumValue } from 'state/actions/sumValue';
import { subValue } from 'state/actions/subValue';
import { timesValue } from 'state/actions/timesValue';
import { divValue } from 'state/actions/divValue';
import { sqrValue } from 'state/actions/sqrValue';
import { sumAllValue } from 'state/actions/sumAllValue';
import { undoValue } from 'state/actions/undoValue';
import { dotValue } from 'state/actions/dotValue';
import { selectCurrentNumber } from 'state/selectors/selectCurrentNumber';
import { selectCurrentStack } from 'state/selectors/selectCurrentStack';

import styles from './Calculator.module.css';

const renderStackItem = (value: number, index: number) => {
  console.log(value)
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);

  const dispatch = useDispatch();
  
  const onClickNumber = (n: number) => {
    const action = getValue(n);
    dispatch(action);
  };
  const onClick = () => {
    console.log('entra')
  };

  const onEnterClick = () => {
    const action = enterValue();
    dispatch(action);
    console.log(stack)
  };

  const onSumClick = () => {
    const action = sumValue();
    dispatch(action);
    console.log(stack)
  };

  const onSubClick = () => {
    const action = subValue();
    dispatch(action);
    console.log(stack)
  };

  const onTimesClick = () => {
    const action = timesValue();
    dispatch(action);
    console.log(stack)
  };

  const onDivClick = () => {
    const action = divValue();
    dispatch(action);
    console.log(stack)
  };

  const onSqrClick = () => {
    const action = sqrValue();
    dispatch(action);
    console.log(stack)
  };

  const onSumAllClick = () => {
    const action = sumAllValue();
    dispatch(action);
    console.log(stack)
  };

  const onUndoClick = () => {
    const action = undoValue();
    dispatch(action);
    console.log(stack)
  };

  const onDotClick = () => {
    const action = dotValue();
    dispatch(action);
    console.log(stack)
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => onClickNumber(i + 1)}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => onClickNumber(0)}>
          0
        </button>
        <button onClick={() => onDotClick()}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => onSumClick()}>+</button>
        <button onClick={() => onSubClick()}>-</button>
        <button onClick={() => onTimesClick()}>x</button>
        <button onClick={() => onDivClick()}>/</button>
        <button onClick={() => onSqrClick()}>√</button>
        <button onClick={() => onSumAllClick()}>Σ</button>
        <button onClick={() => onUndoClick()}>Undo</button>
        <button onClick={() => onEnterClick()}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
